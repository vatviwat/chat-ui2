import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { SocketIoService } from './socket-io.service';
import * as $ from 'jquery';
// @ts-ignore
import typingGif from '../assets/img/typing.gif';
import { CookieService } from 'ngx-cookie-service';
import { NgxImageCompressService } from 'ngx-image-compress';
import { NgxEncryptCookieService } from 'ngx-encrypt-cookie';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'socket-io-chat';
  text;
  id;
  message;
  receiverSessionId;
  receiverId;
  chatWithUser;
  listUser;
  currentUser;
  currentUserId;
  typingTimer;
  receiverName;
  imgResultBeforeCompress;
  imgResultAfterCompress;
  image;
  selectedFile;
  messages = [];
  reply=true
  @ViewChild('inbox_chat') inboxChat: ElementRef;
  @ViewChild('msg_history') msgHistory: ElementRef;

  constructor(
    private socketIoService: SocketIoService,
    private renderer2: Renderer2,
    private cookie: CookieService,
    private imageCompress: NgxImageCompressService,
    private cookieService: NgxEncryptCookieService
  ) {}

  sendChat(event: any) {
    console.log(event.message);
    let senderId =  this.cookieService.get('userId', true, 'hrd')
    this.messages = this.socketIoService.sendMessage(
      senderId,
      this.currentUser,
      this.receiverId,
      event.message,
      this.receiverName,
      this.imgResultAfterCompress,
      this.messages
    );
    //this.displaySenderMessage(this.text);
  }

  ngOnInit(): void {

    let senderId =  this.cookieService.get('userId', true, 'hrd')
    console.log(senderId);
    
  

    this.socketIoService.connect();

    this.socketIoService.getNewMessage(senderId, 2).subscribe((res) => {
      
      this.messages = res.message;

      if(res.message.senderId != senderId)
        this.reply = true
        else
        this.reply=false
    });
      
  
    this.socketIoService.getMessages().subscribe((data) => {

      if(data.message!=null)
      this.messages.push(data.message[data.message.length-1])

    });
  
    this.socketIoService.getUser().subscribe((data) => {
      // this.renderUser(data.username, data.sessionId);
    });
    this.socketIoService.listUser.subscribe((res) => {
      this.listUser = res;
    });
    this.socketIoService.onUserTyping().subscribe((data) => {
      console.log(data);
      this.createTypingTemplate();
    });
    this.socketIoService.onUserStopTyping().subscribe((data) => {
      console.log('stop');
      $('#typing').remove();
    });
    this.socketIoService.currentId.subscribe((res) => {
      console.log(res)
      // @ts-ignore
      this.currentUserId = res.id;
    });
  
  }

  renderUser(name, sessionId): void {
    const chatList = this.renderer2.createElement('div');
    chatList.classList.add('chat_list');
    chatList.addEventListener('click', () => {
      this.receiverSessionId = sessionId;
      this.chatWithUser = name;
    });
    const chatPeople = this.renderer2.createElement('div');
    chatPeople.classList.add('chat_people');
    const chatImg = this.renderer2.createElement('div');
    chatImg.classList.add('chat_img');
    const img = this.renderer2.createElement('img');
    img.src = 'https://bootdey.com/img/Content/avatar/avatar2.png';
    img.alt = 'sunil';
    const chatIb = this.renderer2.createElement('div');
    chatIb.classList.add('chat_ib');
    const h5 = this.renderer2.createElement('h5');
    const text = this.renderer2.createText(name);
    chatList.append(chatPeople);
    chatPeople.append(chatImg);
    chatPeople.append(chatIb);
    chatImg.append(img);
    chatIb.append(h5);
    h5.append(text);
    this.renderer2.appendChild(this.inboxChat.nativeElement, chatList);
  }


  userTyping(): void {
    this.socketIoService.userTyping(this.currentUser, this.receiverName);
    clearTimeout(this.typingTimer);
    this.typingTimer = setTimeout(() => {
      this.socketIoService.userStopTyping(this.currentUser, this.receiverName);
    }, 1000);
  }

  createTypingTemplate(): void {
    if ($('#typing').length === 0) {
      // tslint:disable-next-line:variable-name
      const incoming_msg = this.renderer2.createElement('div');
      incoming_msg.classList.add('incoming_msg');
      // incoming_msg.classList.add('type');
      incoming_msg.setAttribute('id', 'typing');
      // tslint:disable-next-line:variable-name
      const received_msg = this.renderer2.createElement('div');
      received_msg.classList.add('received_msg"');
      const img = this.renderer2.createElement('img');
      img.src = typingGif;
      img.classList.add('typing');
      incoming_msg.append(received_msg);
      received_msg.append(img);
      this.renderer2.appendChild(this.msgHistory.nativeElement, incoming_msg);
    }
  }

  compressFile(): void {
    this.imageCompress.uploadFile().then(({ image, orientation }) => {
      this.imgResultBeforeCompress = image;
      console.warn('Size in bytes was:', this.imageCompress.byteCount(image));

      this.imageCompress
        .compressFile(image, orientation, 50, 50)
        .then((result) => {
          this.imgResultAfterCompress = result;
          console.warn(
            'Size in bytes is now:',
            this.imageCompress.byteCount(result)
          );
        });
    });
  }
}
