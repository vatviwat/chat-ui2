import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { NgxEncryptCookieService } from 'ngx-encrypt-cookie';
import { nbSelectFormFieldControlConfigFactory } from '@nebular/theme';
import { fakeAsync } from '@angular/core/testing';

@Injectable({
  providedIn: 'root',
})
export class SocketIoService {
  socket;
  private userSource = new Subject();
  listUser = this.userSource.asObservable();
  private idSource = new Subject();
  currentId = this.idSource.asObservable();
  message = [];

  constructor(private http: HttpClient, private cookie: CookieService,private cookieService: NgxEncryptCookieService) {}

  connect(): void {
    // @ts-ignore
    this.socket = io('http://127.0.0.1:8888/chat');
    this.socket.on('connect', () => {
    let senderId = this.cookieService.get('userId',true,'hrd')
 
      // register user
     // if (this.cookie.check('user') === false) {
        this.registerUser(senderId, this.socket.id).subscribe((res) => {
          console.log(res)
          this.idSource.next(res);
          this.cookie.set('user', JSON.stringify(res));
          this.socket.emit('userJoin', {
            sessionId: this.socket.id,
            username:'2'
            
          });
        });
     // }
      // after register send event to socket to get user every time user register
      this.socket.emit('userJoin', {
        sessionId: this.socket.id,
        username:'2'
    });
  })}

  // tslint:disable-next-line:variable-name
  sendMessage(
    sender_Id,
    sender_Name,
    receiver_Id,
    text,
    receiver_Name,
    files,
    oldMessages
  ): any {
    // tslint:disable-next-line:prefer-const
    oldMessages.push({
      text: text,
      date: new Date(),
      reply: false,
      //type: files.length ? 'file' : 'text',
      files: '',
      user: {
        username: sender_Name,
        avatar: 'https://i.gifer.com/no.gif',
      },
    });
    this.socket.emit('sendMessage', {
      message: oldMessages,
      senderId: 2,
      senderName: 'sender',
      receiverName: 'receiver',
      receiverId: 14,
      dataImage: null,
      receiverUsername:'14'
    });
    return oldMessages;
  }

  getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('newMessage', (message) => {
        observer.next(message);

      });
    });
  };
  getUser = () => {
    return Observable.create((observer) => {
      this.socket.on('newUser', (data) => {
        observer.next(data);
        this.getAllUser().subscribe((res) => {
          this.userSource.next(res);
          console.log(res);
        });
      });
    });
  };

  getAllUser(): Observable<any> {
    return this.http.get('http://localhost:8080/users');
  }

  onUserTyping = () => {
    return Observable.create((observer) => {
      this.socket.on('userTyping', (data) => {
        observer.next(data);
      });
    });
  };

  getNewMessage(sender,receiver): Observable<any> {
    return this.http.get("http://localhost:8080/messages?room_number=2sender14receiver")
  }

  // tslint:disable-next-line:variable-name
  userTyping(senderName, receiver_Name): void {
    this.socket.emit('userTyping', {
      senderId: this.socket.id,
      sender: senderName,
      receiverName: receiver_Name,
    });
  }

  onUserStopTyping = () => {
    return Observable.create((observer) => {
      this.socket.on('userStopTyping', (data) => {
        observer.next(data);
      });
    });
  };

  // tslint:disable-next-line:variable-name
  userStopTyping(senderName, receiver_Name): void {
    this.socket.emit('userStopTyping', {
      senderId: this.socket.id,
      sender: senderName,
      receiverName: receiver_Name,
    });
  }

  registerUser(currentUser, CurrentsessionId): Observable<any> {
    return this.http.post('http://localhost:8080/users', {
      sessionId: CurrentsessionId,
      userId: currentUser,
    });
  }

  getMessageByRoom(senderNameId, receiverNameId): Observable<any> {
    return this.http.get(
      'http://localhost:8080/messages?senderNameId=' +
        senderNameId +
        '&receiverNameId=' +
        receiverNameId
    );
  }

  uploadImage(image: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', image);
    return this.http.post('http://localhost:8080/upload', formData);
  }
}
